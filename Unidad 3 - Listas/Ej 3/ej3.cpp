/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 3.3
 *
 * Enunciado:
 *
 *     Escribir un programa que permita eliminar elementos de una lista de números aceptada
 *  por teclado. El programa pregunta qué elemento borrar considerando que el primero por la
 *  izquierda es el 1.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <cstdlib>

#include "classLista.hpp"

using namespace std;

int menu();

int main(int argc, char *argv[]) {
    Lista mi_lista;
    int opt;
    int tmp_int;
    char tmp;

    opt = menu();
    while (opt >= 1 && opt <= 4) {
        cout << "Ingrese Valor:" << endl;

        switch (opt) {
        case 1:
            cin >> tmp;
            mi_lista.insertar_principio((char)tmp);
            break;
        case 2:
            cin >> tmp;
            mi_lista.insertar_medio((char)tmp);
            break;
        case 3:
            cin >> tmp;
            mi_lista.insertar_final((char)tmp);
            break;
        case 4:
            cin >> tmp_int;
            cout << "Borrando: " << tmp << endl;
            mi_lista.eliminar_pos(tmp_int);
            break;
        }
        mi_lista.imprimir();
        opt = menu();

    }
    return 0;
}

int menu(void) {
    int tmp;
    cout << "Menu" << endl;
    cout << "1) Agregar al principio de la lista" << endl;
    cout << "2) Agregar al medio de la lista" << endl;
    cout << "3) Agregar al final de la lista" << endl;
    cout << "4) Borrar un elemento" << endl;
    cout << "" << endl;
    cout << "Otra tecla, Salir" << endl;

    cin >> tmp;
    return tmp;
}
