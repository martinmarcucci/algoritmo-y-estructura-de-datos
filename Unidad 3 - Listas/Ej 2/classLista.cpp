#include <iostream>
#include <cstdlib>
#include "classLista.hpp"

Lista::Lista() {
    nlista = NULL; //es el primer nodo
    size = 0;
}

Lista::Lista(char d) { //este constr es cuando se instanciará la clase
    nlista = new nodo(d); //nlista es la cabeza de la lista; llama al constructor de nodo
    size++; //incrementa size en 1
}

//añade valores a la lista en orden correcto

void Lista::insertar(char d) { //el insertar es para el resto de los nodos. Es p una lista simplemente enlazada

    nodo *newNodo = new nodo(d); //acá se crea un nuevo nodo
    nodo *prevNodo = NULL;
    nodo *currNodo = nlista; //nodo actual de la lista

    while (currNodo != NULL && d > currNodo->getDato()) {
        prevNodo = currNodo;
        currNodo = currNodo->getProximo();
    }
    if (prevNodo == NULL) {
        newNodo->insertarNodo(nlista);
        nlista = newNodo;
    } else {
        prevNodo->insertarNodo(newNodo);
        newNodo->insertarNodo(currNodo);
    }
    size++;
}

void Lista::insertar_final(char d) { //el insertar es para el resto de los nodos. Es p una lista simplemente enlazada

    nodo *newNodo = new nodo(d); //acá se crea un nuevo nodo
    nodo *prevNodo = NULL;
    nodo *currNodo = nlista; //nodo actual de la lista

    while (currNodo != NULL ) {
        prevNodo = currNodo;
        currNodo = currNodo->getProximo();
    }

    if (prevNodo == NULL) {
        nlista = newNodo;
    } else {
        prevNodo->insertarNodo(newNodo);
    }
    size++;
}

void Lista::insertar_principio(char d) { //el insertar es para el resto de los nodos. Es p una lista simplemente enlazada

    nodo *newNodo = new nodo(d); //acá se crea un nuevo nodo

    if (nlista != NULL)
        newNodo->insertarNodo(nlista);

    nlista = newNodo;
    size++;
}


void Lista::insertar_medio(char d) { //el insertar es para el resto de los nodos. Es p una lista simplemente enlazada

    nodo *newNodo = new nodo(d); //acá se crea un nuevo nodo
    nodo *prevNodo = NULL;
    nodo *currNodo = nlista; //nodo actual de la lista
    int cont = 0;

    while (currNodo != NULL && cont < size/2) {
        cont++;
        prevNodo = currNodo;
        currNodo = currNodo->getProximo();
    }

    if (prevNodo == NULL) {
        nlista = newNodo;
    } else {
        newNodo->insertarNodo(currNodo);
        prevNodo->insertarNodo(newNodo);
    }
    size++;
}

void Lista::eliminar(char d) {
    nodo *currNodo = nlista;
    nodo *prevNodo = nlista;
    if (!esVacia()) {
        while (currNodo->getProximo() != NULL && d != currNodo->getDato()) {
            prevNodo = currNodo;
            currNodo = currNodo->getProximo();
        }
        prevNodo->insertarNodo(currNodo->getProximo());
        delete (currNodo);
        if (currNodo == nlista)
            nlista = NULL;
        size--;
    }
}

char Lista::buscar(char d) {
    int i = 1;
    nodo *currLista = nlista;

    if (!esVacia()) {
        while (currLista != NULL && d != currLista->getDato()) {
            i++;
            currLista = currLista->getProximo();
        }
        if (i > size)
            return -1;
        else
            return (currLista->getDato());
    } else
        return -2;
}

char Lista::buscarRecursiva(char d, nodo *l) {
    if (l == NULL)
        l = nlista;
    if (l->getProximo() == NULL)
        return -2;
    else if (l->getDato() == d)
        return (l->getDato());
    else
        return buscarRecursiva(d, l->getProximo());
}

void Lista::imprimir() {
    nodo *olista = nlista; //olista actúa como un auxiliar
    if (olista == NULL)
        cout << "Esta vacia" << endl;
    else {
        while (olista != NULL) {
            cout << olista->getDato() << " -> ";
            olista = olista->getProximo();
        }
        cout << "Fin Imprimir" << endl;
    }
}

Lista::~Lista() {
    while (nlista != NULL) {
        eliminar(0);
    }
    size = 0;
}

void Lista::fnInvierte() {
    nodo *aux2, *aux1 = NULL;
    while (nlista != NULL) {
        aux2 = nlista;
        nlista = nlista->getProximo();
        aux2->insertarNodo(aux1);
        aux1 = aux2;
    }
    nlista = aux2;
}

