/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 3.2
 *
 * Enunciado:
 *
 *  Escribir un programa que permita agregar nº enteros a una lista de números aceptada por
 *  teclado. El programa pregunta si debe agregar al principio, al final o en el medio y agrega el
 *  elemento a la lista.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <cstdlib>

#include "classLista.hpp"

using namespace std;

int menu();

int main(int argc, char *argv[]) {
    Lista mi_lista;
    int opt;
    char tmp;

    opt = menu();
    while (opt >= 1 && opt <= 3) {
        cout << "Ingrese Valor:" << endl;
        cin >> tmp;

        switch (opt) {
        case 1:
            mi_lista.insertar_principio(tmp);
            break;
        case 2:
            mi_lista.insertar_medio(tmp);
            break;
        case 3:
            mi_lista.insertar_final(tmp);
            break;
        }
        mi_lista.imprimir();
        opt = menu();

    }

    // system("PAUSE");
    // delete &mi_lista;
    return 0;
}

int menu(void) {
    int tmp;
    cout << "Menu" << endl;
    cout << "1) Agregar al principio de la lista" << endl;
    cout << "2) Agregar al medio de la lista" << endl;
    cout << "3) Agregar al final de la lista" << endl;
    cout << "" << endl;
    cout << "Otra tecla, Salir" << endl;

    cin >> tmp;
    return tmp;
}
