// class lista
#include <cstdlib>
#include <iostream>

#include "nodo.hpp"

using namespace std;

#ifndef CLASSLISTA_HPP
#define CLASSLISTA_HPP

class Lista {
private:
    nodo *nlista; //tipo puntero
    int size; //es para saber la cant de nodos con la q estamos trabajando. EL size se llena en el "insertar"

public:
    Lista(); //constructor
    Lista(char d); //constructor (le pasa el valor del dato, que lo usa despu�s el constructor de la clase nodo
    Lista(Lista *la, Lista *lb);

    int esVacia() {
        return nlista == NULL;
    }; //revisa si el 1er nodo o el proximo es NULL
    void insertar(char d); //inserta un nodo pero ordenadamente
    void insertar_final(char d); //inserta un nodo pero ordenadamente
    void insertar_principio(char d); //inserta un nodo pero ordenadamente
    void insertar_medio(char d); //inserta un nodo pero ordenadamente
    char buscar(char d); //retorna -1 si no se encuentra el dato,-2 si la lista esta vacia o el dato en caso de que se encuentre en la lista
    char buscarRecursiva(char d, nodo *l);
    void eliminar_pos(int d) ;
    nodo* get_nodo_pos(int d) ;
    void eliminar(char d); //similar al insertar; elimina y libera memoria
    void imprimir();
    void fnInvierte(); //invierte la lista

    long tamanio() {
        return size;
    }; //devuelve el tama�o

    nodo *getNodo() {
        return nlista;
    }; //devuelve un tipo de nodo; retorna el 1er elemento de la lista
    ~Lista();
};


#endif
