/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 3.4
 *
 * Enunciado:
 *   Crear una función que dada dos listas enlazadas, pasadas como parámetro, devuelva una
 *     lista enlazada que es la unión de las otras dos.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <cstdlib>

#include "classLista.hpp"

using namespace std;

int crea_lista(Lista *mi_lista);

int main(int argc, char *argv[]) {
    Lista lista1;
    Lista lista2;
    Lista *final;

    crea_lista(&lista1);
    crea_lista(&lista2);

    final = new Lista(&lista1, &lista2);

    final->imprimir();

    return 0;
}

int crea_lista(Lista *mi_lista) {
    int opt;
    int tmp_int;
    int tmp;

    while (opt >= 1 && opt <= 4) {

        cout << "Menu" << endl;
        cout << "1) Agregar al principio de la lista" << endl;
        cout << "2) Agregar al medio de la lista" << endl;
        cout << "3) Agregar al final de la lista" << endl;
        cout << "4) Borrar un elemento" << endl;
        cout << "" << endl;
        cout << "Otra tecla, Salir" << endl;

        cin >> opt;
        cout << "Ingrese Valor:" << endl;

        switch (opt) {
        case 1:
            cin >> tmp;
            mi_lista->insertar_principio((char)tmp);
            break;
        case 2:
            cin >> tmp;
            mi_lista->insertar_medio((char)tmp);
            break;
        case 3:
            cin >> tmp;
            mi_lista->insertar_final((char)tmp);
            break;
        case 4:
            cin >> tmp_int;
            cout << "Borrando: " << tmp << endl;
            mi_lista->eliminar_pos(tmp_int);
            break;
        }
        mi_lista->imprimir();
    }
    return 0;
}
