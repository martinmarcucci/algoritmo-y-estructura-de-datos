/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 3.1
 *
 * Enunciado:
 *
 * Implementar la función fnInvierte(lista). Esta función invertirá el orden
 *  original de los elementos en la lista, de tal forma que el último elemento será
 *  ahora el primero, el penúltimo será el segundo, y así hasta que el primero sea
 *  el último. Considere que la lista no está vacía y que no se construirá una
 *  nueva, sólo se invertirá el orden de los elementos de la lista original.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */



#include <iostream>
#include <cstdlib>

#include "classLista.hpp"

using namespace std;

int main(int argc, char *argv[]) {
    Lista mi_lista;

    for (int i = 0; i < 26; i++) {
        mi_lista.insertar(i+65);
    }

    mi_lista.imprimir();
    mi_lista.fnInvierte();
    mi_lista.imprimir();

    // system("PAUSE");
    // delete &mi_lista;
    return 0;
}
