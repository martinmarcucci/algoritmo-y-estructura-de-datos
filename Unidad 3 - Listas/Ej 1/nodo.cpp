#include <iostream>
#include <cstdlib>
#include "nodo.hpp"

using namespace std;



nodo::nodo() {
    dato = 0;
    proximo = NULL;
}

nodo::nodo(char d) {
    this->dato = d;
    this->proximo = NULL;
}

void nodo::insertarNodo(nodo *nuevoNodo) {
    proximo = nuevoNodo;
}

char nodo::getDato() {
    return dato;
}

nodo *nodo::getProximo() {
    return proximo;
}
