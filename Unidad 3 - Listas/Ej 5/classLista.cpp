/*
*             ALGORITMOS Y ESTRUCTURA DE DATOS
* Ejercicio: 3.5
*
* Enunciado:
*   Escriba la clase CircList para implementar una lista circular.
*
* Alumno: Ricardo Martin Marcucci
*/


#include <iostream>
#include <cstdlib>
#include "classLista.hpp"

ListaCirc::ListaCirc() {
    nlista = NULL; //es el primer nodo
    size = 0;
}

ListaCirc::ListaCirc(char d) { //este constr es cuando se instanciar la clase
    nlista = new nodo(d); //nlista es la cabeza de la lista; llama al constructor de nodo
    nlista->insertarNodo(nlista);
    size++; //incrementa size en 1
}

nodo* ListaCirc::get_nodo_pos(int d) {
    nodo* aux = nlista;
    int num = 0;

    while( num < d || aux != NULL) {
        aux = aux->getProximo();
    }

    return aux;
}

void ListaCirc::insertar_final(char d) { //el insertar es para el resto de los nodos. Es p una lista simplemente enlazada

    nodo *newNodo = new nodo(d); //acÃ¡ se crea un nuevo nodo
    nodo *prevNodo = NULL;
    nodo *currNodo = nlista; //nodo actual de la lista

    while (prevNodo->getProximo() != nlista) {
        prevNodo = currNodo;
        currNodo = currNodo->getProximo();
    }

    if (prevNodo == NULL) {
        nlista = newNodo;
    } else {
        prevNodo->insertarNodo(newNodo);
        newNodo->insertarNodo(nlista);
    }
    size++;
}

/*
 * Elimina por posicion
 */
void ListaCirc::eliminar_pos(int d) {
    nodo *currNodo = nlista;
    nodo *prevNodo = NULL;
    nodo *prev2Nodo = NULL;

    int i = 0;

    if ( !esVacia() ) {
        while (currNodo != NULL && i < d) {
            i++;
            prev2Nodo = prevNodo;
            prevNodo = currNodo;
            currNodo = currNodo->getProximo();
        }
        if (i == d) {
            if( prev2Nodo != NULL )
                prev2Nodo->insertarNodo( currNodo );
            else if ( prevNodo == nlista )
                nlista = NULL;
            delete ( prevNodo );
        }
        size--;
    }
}

/*
 * Elimina por valor
 */
void ListaCirc::eliminar(char d) {
    nodo *currNodo = nlista;
    nodo *prevNodo = nlista;
    if (!esVacia()) {
        while (currNodo->getProximo() != NULL && d != currNodo->getDato()) {
            prevNodo = currNodo;
            currNodo = currNodo->getProximo();
        }
        prevNodo->insertarNodo(currNodo->getProximo());
        delete (currNodo);
        if (currNodo == nlista)
            nlista = NULL;
        size--;
    }
}

char ListaCirc::buscar(char d) {
    int i = 1;
    nodo *currLista = nlista;

    if (!esVacia()) {
        while (currLista != NULL && d != currLista->getDato()) {
            i++;
            currLista = currLista->getProximo();
        }
        if (i > size)
            return -1;
        else
            return (currLista->getDato());
    } else
        return -2;
}

char ListaCirc::buscarRecursiva(char d, nodo *l) {
    if (l == NULL)
        l = nlista;
    if (l->getProximo() == NULL)
        return -2;
    else if (l->getDato() == d)
        return (l->getDato());
    else
        return buscarRecursiva(d, l->getProximo());
}

void ListaCirc::imprimir() {
    nodo *olista = nlista; //olista actÃºa como un auxiliar
    if (olista == NULL)
        cout << "Esta vacia" << endl;
    else {
        while (olista != NULL) {
            cout << olista->getDato() << " -> ";
            olista = olista->getProximo();
        }
        cout << "Fin Imprimir" << endl;
    }
}

ListaCirc::~ListaCirc() {
    while (nlista != NULL) {
        this->eliminar_pos(1);
    }
    size = 0;
}

void ListaCirc::fnInvierte() {
    nodo *aux2, *aux1 = NULL;
    while (nlista != NULL) {
        aux2 = nlista;
        nlista = nlista->getProximo();
        aux2->insertarNodo(aux1);
        aux1 = aux2;
    }
    nlista = aux2;
}

