
#ifndef NODO_HPP
#define NODO_HPP
class nodo {
    char dato;
    nodo *proximo;

public:

    nodo() ;
    nodo(char d);
    char getDato();

    nodo *getProximo();
    void setDato(char d);
    void insertarNodo(nodo *nuevoNodo);
};


#endif
