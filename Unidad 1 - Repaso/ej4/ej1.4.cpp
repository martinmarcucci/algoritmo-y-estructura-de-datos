/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 1.4
 *
 * Enunciado:
 *
 * Dado un mes del a�o, su n�mero de d�as y el d�a de la semana
 * en que comienza, realizar un programa que muestre por pantalla
 * la representaci�n del calendario correspondiente a dicho mes.
 *
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    int mes;
    int dias = 0;
    int anio;
    int inicio_semana;

    char semana[15];

    cout << "Ingrese el  mes:";
    cin >> mes;

    cout << "\nIngrese el anio:";
    cin >> anio;

    cout << "\nEn que dia de la semana inicia el mes?:";
    cin >> semana;

    switch (mes) {
        // Meses con 31 dias
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        dias += 1;
        // Meses con 30 dias
    case 4:
    case 6:
    case 9:
    case 11:
        dias += 30;
        break;
        // Febrero, con 28 o 29
    case 2:
        if (anio % 4 == 0)
            dias += 29;
        else

            dias += 28;
    }

    cout << "\n\n" << mes << " del " << anio;
    cout << "\nD\tL\tM\tM\tJ\tV\tS";
    cout << "\n__________________________________________________\n";

    semana[0] = toupper(semana[0]);
    semana[1] = toupper(semana[1]);

    switch (semana[0]) {
    case 'D':
        inicio_semana = 0;
        break;
    case 'L':
        inicio_semana = 1;
        break;
    case 'M':
        if (semana[1] == 'A')
            inicio_semana = 2;
        else
            inicio_semana = 3;
        break;
    case 'J':
        inicio_semana = 4;
        break;
    case 'V':
        inicio_semana = 5;
        break;
    case 'S':
        inicio_semana = 6;
        break;

    }

    for (int i = 1; i <= dias + inicio_semana; i++) {

        if (i - inicio_semana > 0)
            cout << i - inicio_semana;

        cout << "\t";
        if (i % 7 == 0)
            cout << "\n";
    }
    return 0;
}
