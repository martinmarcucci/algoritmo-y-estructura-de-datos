/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 1.1
 *
 * Enunciado:
 * Escribir un programa que lea una fecha en formato DD-MM-AAAA y
 * calcule el n�mero de d�as transcurridos desde el d�a 1 de Enero
 * de ese a�o.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <cstdlib>
using namespace std;

/*
 *
 */
int main(int argc, char** argv) {
    int dia;
    int mes;
    int anio;
    int total = 0;

    cout << "Ingrese La fecha:\nDia:";
    cin >> dia;
    cout << "\nMes:";
    cin >> mes;
    cout << "\nAnio:";
    cin >> anio;

    total += dia;
    while (mes-- > 0) {
        switch (mes) {
            // Meses con 31 dias
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            total += 1;
            // Meses con 30 dias
        case 4:
        case 6:
        case 9:
        case 11:
            total += 30;
            break;
            // Febrero, con 28 o 29
        case 2:
            if (anio % 4 == 0)
                total += 29;
            else

                total += 28;
        }
    }
    cout << "Pasaron " << total << " dias desde el 1 de Enero de " << anio << "\n";
    return 0;
}
