/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 1.5
 *
 * Enunciado:
 *
 * Realizar una aplicaci�n para gestionar cierta informaci�n de los
 * empleados de una empresa. El dise�o (incompleto) de la aplicaci�n
 * estar� formado por dos clases: la clase empleado y la clase
 * empleadoPorHoras (que hereda de empleado), junto con un programa
 * principal que llama a las operaciones p�blicas de ambas clases.
 *
 * Clase empleado: contiene como datos privados el nombre del empleado
 *  y su n�mero de DNI. Adem�s, sus m�todos p�blicos son: un constructor
 *  de la clase, dos operaciones selectoras (GetNombre y GetDNI), y una
 *  operaci�n destructora de la clase.
 *
 * Clase empleadoPorHoras: contiene como datos privados el coste horario
 *  de un empleado y su n�mero de horas trabajadas durante la semana. Adem�s,
 *  dispone de los siguientes m�todos p�blicos: un constructor de la clase
 *  (que requiere de su clase base el nombre y el DNI del empleado) y una
 *  operaci�n llamada DevuelvePaga (que calcula la paga del empleado
 *  multiplicando su coste horario por el n�mero de horas trabajadas).
 *
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <string>

using namespace std;

class empleado {
public:
    empleado(string nom, int dni_num) {
        nombre= nom;
        dni=dni_num;
    };
    ~empleado();

    string GetNombre() {
        return nombre;
    };

    int GetDNI() {
        return dni;
    };

private:
    string nombre;
    int dni;
};

class empleadoPorHoras : public empleado {
public:
    empleadoPorHoras(string nombre, int dni, int costo, int horas): empleado(nombre,dni) {

        costo_horario=costo;
        horas_trabajadas=horas;
    };
    ~empleadoPorHoras();

    int DevuelvePaga() {
        return costo_horario * horas_trabajadas;
    };

private:
    int costo_horario;
    int horas_trabajadas;
};

int main(int argc, char** argv) {
    empleadoPorHoras *emp;
    emp = new empleadoPorHoras("martin",123456,10,20);
    cout << "Paga total de "<< emp->GetNombre() << " :"  << emp->DevuelvePaga() << "$\n";
}
