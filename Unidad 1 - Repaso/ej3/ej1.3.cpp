/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 1.3
 *
 * Enunciado:
 * Calcular el tiempo equivalente en horas minutos y segundos a
 * un número de segundos leído. El resultado debe imprimirse en
 * un formato como el siguiente:
 *
 *    7322 segundos equivalen a: 2 horas, 2 minutos y 2 segundos.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    int seg;
    int min;
    int horas;
    int total;

    cout << "\nIngrese los segundos:";
    cin >> total;

    horas = (int) (total / 3600);
    min = (int) (total % 3600) / 60;
    seg = (int) (total % 60);

    cout << total << "s, son: " << horas << " horas, " << min << " minutos y " << seg << " segundos";

    return 0;
}
