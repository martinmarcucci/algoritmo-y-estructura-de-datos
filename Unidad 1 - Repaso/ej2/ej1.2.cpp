/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 1.2
 *
 * Enunciado:
 * Imprimir el mayor, el menor, la suma de diez números aceptados por teclado.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */


#include <iostream>
#include <cstdlib>
using namespace std;

using namespace std;

int main(int argc, char** argv) {
    int numero;
    int max = -999999999;
    int min = 999999999;
    int i = 10;
    int suma = 0;

    do {
        cout << "\n Ingrese un numero (" << i - 1 << " restantes):";
        cin >> numero;
        // Chequeo el maximo
        max = (numero > max) ? numero : max;
        // Chequeo el minimo
        min = (numero < min) ? numero : min;
        // Hago la suma
        suma = suma + numero;
        i--;
    } while (i > 0);

    cout << "\nMax:" << max << "\tMin:" << min << "\tSuma:" << suma << "\n";
    return 0;
}
