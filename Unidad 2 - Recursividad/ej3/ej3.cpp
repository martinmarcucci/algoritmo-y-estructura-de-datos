/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 2.3
 *
 * Enunciado:
 * Algoritmo de Euclides: mostrar que el máximo común divisor (mcd) de a y b, (a > b > 0),
 * es igual a a si b es cero, en otro caso es igual al mcd de b y el remanente de a
 * dividido por b, si b > 0.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */
#include <cstdlib>
#include <iostream>

using namespace std;

void mcd(int a, int b);


int main(int argc, char *argv[]) {

    mcd(50, 40);
    system("PAUSE");
    return EXIT_SUCCESS;
}

void mcd(int a, int b) {
    if (b == 0)
        cout << a << endl;
    else {
        cout << a << " " << b << endl;
        mcd(b, a % b);
    }
}