/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 2.4
 *
 * Enunciado:
 *           Escribir segmentos de programa que lleven a cabo, cada uno de
 *	     ellos, lo siguiente:
 *
 * a)  Calcule la parte entera del cociente, cuando el entero a se divide por el
 *     entero b.
 *
 * b)  Calcule el residuo entero, cuando el entero a es dividido por el entero
 *     b.
 *
 * c)  Utilice los módulos a) y b) para escribir una función que introduzca un
 *     entero no negativo por teclado y lo imprima como una serie de dígitos separados
 *     por espacios.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */
#include <cstdlib>
#include <iostream>

using namespace std;

int cociente(int dividendo, int divisor);
int residuo(int dividendo, int divisor);
void escribir(int num, int val);

int main(int argc, char *argv[]) {
    int numero;

    do {
        cout << "Ingrese un numero: ";
        cin >> numero;
    } while (numero < 0);

    escribir(numero, 1000000000);
    system("pause");
    return EXIT_SUCCESS;
}

int cociente(int dividendo, int divisor) {
    if (dividendo < divisor)
        return 0;
    else
        return 1 + cociente(dividendo - divisor, divisor);

}

int residuo(int dividendo, int divisor) {
    if (dividendo < divisor)
        return dividendo;
    else
        return residuo(dividendo - divisor, divisor);

}

void escribir(int num, int val) {
    int coci, rest;

    if (val <= 0) {
        cout << endl;
    } else {
        coci = cociente(num, val);
        rest = residuo(num, val);
        if (rest != num)
            cout << coci << " ";
        escribir(rest, val / 10);
    }


}
