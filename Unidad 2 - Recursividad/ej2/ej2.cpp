/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 2.2
 *
 * Enunciado:
 * Implemente una función recursiva que, teniendo un array ingresado por
 * teclado, me devuelva la suma de todos sus elementos.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */


#include <cstdlib>
#include <iostream>
#include <stdio.h>

using namespace std;


int sumarray(int numeros[], int cant);

int main(int argc, char *argv[]) {
    int *arr;
    int cantidad;

    cout << "Ingrese la cantidad de numeros a ingresar: ";
    cin >> cantidad;
    arr = new int[cantidad];

    for (int i = 0; i < cantidad; i++) {
        cout << "numero: ";
        cin >> arr[i];
    }

    cout << "TOTAL = " << sumarray(arr, cantidad) << endl;

    system("PAUSE");
    return EXIT_SUCCESS;
}

int sumarray(int numeros[], int cant) {
    if (cant - 1 == 0)
        return numeros[cant - 1];
    else
        return numeros[cant - 1] + sumarray(numeros, cant - 1);
}