/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 2.5
 *
 * Enunciado:
 *
 * 		Modificar la búsqueda Binaria vista en clase de manera tal que sea una función recursiva.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <cstdlib>
#include <iostream>

using namespace std;

int b_binaria(int A[], int inicio, int fin, int valor) ;


int main(int argc, char *argv[]) {

    int test[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89};

    cout << "Pocicion del 29 = " << b_binaria(test, 0, 22, 29);

    system("PAUSE");
    return EXIT_SUCCESS;
}

int b_binaria(int A[], int inicio, int fin, int valor) {
    int pos;
    if (inicio > fin) {
        return -1;
    } else {
        pos = (inicio + fin) / 2;
        if (valor < A[pos])
            return b_binaria(A, inicio, pos - 1, valor);
        else if (valor > A[pos])
            return b_binaria(A, pos + 1, fin, valor);
        else
            return pos;
    }
}
