/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 2.1
 *
 * Enunciado:
 * .Implemente una función para potencias enteras con recursividad.
 *
 * Definición recursiva para elevar un número a una potencia: Un número elevado
 * a la potencia cero produce la unidad; la potencia de un número se obtiene
 * multiplicándolo por sí mismo elevando a la potencia menos uno.
 *
 * Por ejemplo: 32 = 3*(31) = 3*(3*30) = 3*(3*1)= 3*(3) = 9
 *
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <cstdlib>
#include <iostream>

using namespace std;

int potencia(int base, int exponente);


int main(int argc, char *argv[]) {
    int base = 1;
    int expo = 0;
    cout << "ingrese la base de la operacion: ";
    cin >> base;
    cout << "ingrese el exponente: ";
    cin >> expo;

    cout << "resultado: " << potencia(base, expo) << endl;

    system("PAUSE");
    return EXIT_SUCCESS;
}

int potencia(int base, int exponente) {

    if (0 == exponente) {
        return 1;
    } else {
        return base * potencia(base, exponente - 1);
    }

}
