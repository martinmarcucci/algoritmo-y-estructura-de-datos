/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 8.2
 *
 * Enunciado:
 * Probar el algoritmo visto en clase agregandole una funcion main.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <cstdlib>
#include <cmath>


#include "HashMap.hpp"

using namespace std;

int main(int argc, char** argv) {
    HashMap myMap;
    for(int i = 0; i < 128; i++)
        myMap.put(i*i*i, i);

    myMap.print_tabla();


}
