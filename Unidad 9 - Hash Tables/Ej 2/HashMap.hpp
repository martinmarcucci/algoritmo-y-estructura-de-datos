#include <iostream>
#include <string>
#include "LinkedHashEntry.hpp"

using namespace std;

#ifndef HASHMAP_HPP
#define HASHMAP_HPP

const int TABLE_SIZE = 128;

class HashMap {

private:

    LinkedHashEntry **table;

public:
    HashMap();

    int get(int key);
    void put(int key, int value);
    void remove(int key);
    ~HashMap();
    void print_tabla();
};

#endif
