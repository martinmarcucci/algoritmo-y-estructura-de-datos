#include <iostream>
#include <string>


#ifndef LINKEDHASHENTRY_HPP
#define LINKEDHASHENTRY_HPP


class LinkedHashEntry {

private:
    int key;
    int value;
    LinkedHashEntry *next;

public:
    LinkedHashEntry(int key, int value);
    int getKey();
    int getValue();
    void setValue(int value);
    LinkedHashEntry *getNext();
    void setNext(LinkedHashEntry *next);
};

#endif