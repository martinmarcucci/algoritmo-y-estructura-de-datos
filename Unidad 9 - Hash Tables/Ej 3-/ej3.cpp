/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 8.1
 *
 * Enunciado:
 * Probar el algoritmo visto en clase agregandole una funcion main.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include <cstdlib>


#include "HashMap.hpp"

using namespace std;

int main(int argc, char** argv) {
    HashMap myMap;
    for(int i = 0; i < 128; i++)
        myMap.put(128*i, i);

    myMap.print_tabla();


}
