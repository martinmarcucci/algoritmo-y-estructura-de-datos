#include <iostream>
#include <string>

#include "HashEntry.hpp"
#include "HashMap.hpp"

using namespace std;

HashMap::HashMap() {
    table = new HashEntry*[TABLE_SIZE];
    for (int i = 0; i < TABLE_SIZE; i++)
        table[i] = NULL;
}

int HashMap::get(int key) {
    int hash = (key % TABLE_SIZE);
    if (table[hash] == NULL)
        return -1;
    else {
        HashEntry *entry = table[hash];
        while (hash < TABLE_SIZE && entry != NULL && entry->getKey() != key) {

            entry = table[hash];
            hash++;
        }
        if (entry == NULL)
            return -1;
        else
            return entry->getValue();
    }
}

int HashMap::put(int key, int value) {
    int hash = (key % TABLE_SIZE);
    if (table[hash] == NULL)
        table[hash] = new HashEntry(key, value);
    else {
        do {
            hash++;
        } while (hash < TABLE_SIZE - 1 && table[hash] != NULL);

        if (table[hash] == NULL)
            table[hash] = new HashEntry(key, value);
        else
            return -1;
    }
    return 1;
}

int HashMap::remove(int key) {
    int hash = (key % TABLE_SIZE);
    if (table[hash] != NULL) {
        if (table[hash]->getKey() == key)
            delete table[hash];
        else {
            while (hash < TABLE_SIZE && table[hash] != NULL
                    && table[hash]->getKey() != key) {
                hash++;
            }
            if (table[hash]->getKey() == key)
                delete table[hash];
            else
                return -1;
        }
    }
    return 1;
}

HashMap::~HashMap() {
    for (int i = 0; i < TABLE_SIZE; i++)
        if (table[i] != NULL) {
            delete table[i];
        }

    delete[] table;

}

void HashMap::print_tabla() {
    for (int i = 0; i < TABLE_SIZE; i++) {
        cout << "Pos " << i << " = ";
        if (table[i] != NULL) {
            cout << "key " << table[i]->getKey() << " Val "
                 << table[i]->getValue() << endl;
        } else {
            cout << "NULL" << endl;
        }
    }

}

