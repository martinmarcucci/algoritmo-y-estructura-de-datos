#include <iostream>
#include <string>

#ifndef HASHENTRY_HPP
#define HASHENTRY_HPP
class HashEntry {

private:
    int key;
    int value;

public:
    HashEntry(int k, int v);
    int getKey();
    int getValue();
};

#endif
