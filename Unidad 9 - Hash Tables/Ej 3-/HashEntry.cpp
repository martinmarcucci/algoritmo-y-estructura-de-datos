#include <iostream>
#include <string>

#include "HashEntry.hpp"


HashEntry::HashEntry(int key, int value) {
    this->key = key;
    this->value = value;
}



int HashEntry::getKey() {
    return key;

}

int HashEntry::getValue() {
    return value;
}


