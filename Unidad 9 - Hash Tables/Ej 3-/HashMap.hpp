#include <iostream>
#include <string>
#include "HashEntry.hpp"

using namespace std;

#ifndef HASHMAP_HPP
#define HASHMAP_HPP

const int TABLE_SIZE = 128;

class HashMap {

private:

    HashEntry **table;

public:
    HashMap();

    int get(int key);
    int put(int key, int value);
    int remove(int key);
    ~HashMap();
    void print_tabla();
};

#endif
