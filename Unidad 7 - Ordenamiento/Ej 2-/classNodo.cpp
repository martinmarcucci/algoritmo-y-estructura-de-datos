//class nodo


#include <iostream>
#include <stdio.h>
#include <cstdlib>

using namespace std;

class nodo {
    int dato;
    nodo *izquierdo;
    nodo *derecho;

public:
    nodo();
    nodo(int d);
    int getDato() {
        return dato;
    }
    nodo *getDER() {
        return derecho;
    }
    nodo *getIZQ() {
        return izquierdo;
    }    
    void setDER(int d);
    void setIZQ(int d);
    nodo *setDato(int d);
};

nodo :: nodo () {
    this->dato=0;
    this->izquierdo=NULL;
    this->derecho = NULL;
}

nodo :: nodo (int d) {
    this->dato = d;
    this->izquierdo = NULL;
    this->derecho = NULL;
}


 void nodo::setDER(int d) {
        derecho = new nodo(d);
 }
 void nodo::setIZQ(int d) {
        izquierdo = new nodo(d);
 }
