#include <cstdlib>
#include <time.h>       /* time */

#include "classArbol.cpp"

void menu() {
    cout<<"Precione la opcion deseada:" <<endl<<endl;
    cout<<"1-Insertar enteros en el Arbol." <<endl;
    cout<<"2-Eliminar datos del Arbol." <<endl;
    cout<<"3-Cantidad de nodos." <<endl;
    cout<<"4-Imprimir inOrden." <<endl;
    cout<<"5-Imprimir preOrden." <<endl;
    cout<<"6-Imprimir posOrden." <<endl;
    cout<<"0-Salir." <<endl;
}



int main() {
    int itemMenu = -1;
    arbol *oArbol = new arbol();
    int datoArbol, retorno, nuevoDato;
    int array[30];   
    
    srand (time(NULL));
    
    for (int i=0;i<30;i++){
		array[i] = rand() % 400 + 100;
		cout << "-"<< array[i] << "-";
	}
	cout << endl;
	
    for (int i=0;i<30;i++){
            oArbol->hacerArbol(oArbol->getRaiz(), array[i]);
		
	}
		
	oArbol->imprimirInOrden(oArbol->getRaiz());
cout << endl;

    return 0;

}
