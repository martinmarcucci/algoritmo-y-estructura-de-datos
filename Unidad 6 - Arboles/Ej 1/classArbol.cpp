// class arbol
#include <iostream>

#include "classArbol.hpp"
#include "classNodo.hpp"
using namespace std;

arbol::arbol() {
    raiz = NULL;
    contador = 0;
}


void arbol::hacerRaiz(int d) {
    raiz = new nodo(d);
}

/*inserta un nodo en un arbol de binario*/
void arbol::hacerArbol(nodo *r, int d) {
    if(r == NULL && esVacio()) {
        hacerRaiz(d);
    } else {
        int dato = r->getDato();

        if(dato > d) {

            if( r->getIZQ() == NULL ) {
                r->setIZQ(d);
                contador++;
            } else
                hacerArbol(r->getIZQ(), d);
        } else if (dato < d) {
            if( r->getDER() == NULL ) {
                r->setDER(d);
                contador++;
            } else
                hacerArbol(r->getDER(), d);
        } else
            cout<<"duplicado"<<endl;
    }
}




void arbol::imprimirInOrden(nodo *r) {
    if (r->getIZQ())
        imprimirInOrden(r->getIZQ());
    cout<<"-"<<r->getDato()<<"-";
    if (r->getDER())
        imprimirInOrden(r->getDER());
}

void arbol::imprimirPostOrden(nodo *r) {
    if (r->getIZQ()) imprimirPostOrden(r->getIZQ());
    if (r->getDER()) imprimirPostOrden(r->getDER());
    cout<<"-"<<r->getDato()<<"-";
}

void arbol::imprimirPreOrden(nodo *r) {
    cout<<"-"<<r->getDato()<<"-";
    if (r->getIZQ()) imprimirPreOrden(r->getIZQ());
    if (r->getDER()) imprimirPreOrden(r->getDER());
}


void arbol::cambiarNodo(nodo *r, nodo *aux) {
    if (r->getDER() != NULL) cambiarNodo(r->getDER(), aux);
    else {
//    aux->getDato() = r->getDato();
        aux = r;
        r = r->getIZQ();
    }
}



/*elimina un nodo del arbol*/
void arbol::eliminar(nodo *r ,int d) {
    nodo *aux;
    if (esVacio())
        return;

    if(r->getDato()<d)
        eliminar(r->getIZQ(), d);
    else  if(r->getDato()>d)
        eliminar(r->getDER(), d);
    else if (r->getDato()==d) {
        aux = r;
        if (r->getIZQ() == NULL)
            r = r->getDER();
        else if (r->getDER() == NULL)
            r = r->getIZQ();

        else cambiarNodo(r->getIZQ(), aux); /* se sustituye por la mayor de las menores */

        free(aux);
    }
}
