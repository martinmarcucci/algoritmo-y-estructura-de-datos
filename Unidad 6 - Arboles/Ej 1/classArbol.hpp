#include "classNodo.hpp"


#ifndef CLASSARBOL_H
#define CLASSARBOL_H
class arbol {
private:
    nodo *raiz;
    int contador;

public:
    arbol();
    int esVacio() {
        return raiz==NULL;
    };
    void hacerRaiz(int d);
    void hacerArbol(nodo *r, int d);
    void insertar(nodo *r, int d);
    void eliminar(nodo *r, int d);
    int esHoja(nodo *r) {
        return !r->getDER() && !r->getIZQ();
    }
    void imprimirInOrden(nodo *r);
    void imprimirPreOrden(nodo *r);
    void imprimirPostOrden(nodo *r);
    int getNroNodos() {
        return contador;
    }
    int getAltura(nodo *r);
    void cambiarNodo(nodo *r, nodo *x);
    nodo *getRaiz() {
        return raiz;
    };
    ~arbol();
};

#endif