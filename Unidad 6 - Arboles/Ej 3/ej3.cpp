/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 6.3
 *
 * Enunciado:
 *    Especificar la operación contarPorNivel que devuelve el número de nódos del nivel iésimo de
 *	un árbol binario.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */


#include <cstdlib>
#include <time.h>       /* time */

#include "classArbol.hpp"

using namespace std;

int main() {
    arbol *oArbol = new arbol();
    int array[30];

    srand (time(NULL));

    cout << "Generado:\n";
    for (int i=0; i<30; i++) {
        array[i] = rand() % 400 + 100;
        cout << "-"<< array[i] << "-";
    }
    cout << endl;


    cout << "Arbol en orden:\n";
    for (int i=0; i<30; i++) {
        oArbol->hacerArbol(oArbol->getRaiz(), array[i]);

    }

    cout << oArbol->contarPorNivel(oArbol->getRaiz(),3,0);
    cout << endl;

    return 0;

}
