#include <iostream>
#include <stdio.h>
#include <cstdlib>

#ifndef CLASSNODO_H
#define CLASSNODO_H

class nodo {
    int dato;
    nodo *izquierdo;
    nodo *derecho;

public:
    nodo();
    nodo(int d);
    int getDato() {
        return dato;
    }
    nodo *getDER() {
        return derecho;
    }
    nodo *getIZQ() {
        return izquierdo;
    }
    void setDER(int d);
    void setIZQ(int d);
    nodo *setDato(int d);
};

#endif