/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 6.1
 *
 * Enunciado:
 *
 *    Diseñar una aplicación que permita:
 *		a) Cargar en un array 30 números generados aleatoriamente entre 100 y 500.
 *		b) Imprimir los números.
 *		c) Crear un árbol binario de búsqueda con los datos del array.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */


#include <cstdlib>
#include <time.h>       /* time */

#include "classArbol.hpp"
#include "classNodo.hpp"

using namespace std;
void hacer_espejo(arbol &arb,nodo n);

int main() {
    arbol *oArbol = new arbol();
    int array[30];

    srand (time(NULL));

    cout << "Generado:\n";
    for (int i=0; i<30; i++) {
        array[i] = rand() % 400 + 100;
        cout << "-"<< array[i] << "-";
    }
    cout << endl;


    cout << "Arbol en orden:\n";
    for (int i=0; i<30; i++) {
        oArbol->hacerArbol(oArbol->getRaiz(), array[i]);

    }

    oArbol->imprimirInOrden(oArbol->getRaiz());
    cout << endl;

    return 0;

}