//class nodo


#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include "classNodo.hpp"
using namespace std;


nodo :: nodo () {
    this->dato=0;
    this->izquierdo=NULL;
    this->derecho = NULL;
}

nodo :: nodo (int d) {
    this->dato = d;
    this->izquierdo = NULL;
    this->derecho = NULL;
}


void nodo::setDER(int d) {
    derecho = new nodo(d);
}
void nodo::setIZQ(int d) {
    izquierdo = new nodo(d);
}
