#include <cstdlib>
#include <time.h>       /* time */

#include "classArbol.cpp"

/*
 * 5.2.Reconstruir un árbol binario a partir de los recorridos siguientes:
	a)   Preorden: 2, 5, 3, 9, 7, 1, 6, 4, 8.
		  Inorden: 9, 3, 7, 5, 1, 2, 6, 8, 4.
	b)    Inorden: 5, 6, 12, 10, 1, 9, 13, 4, 8, 2, 7, 3, 11.
		Postorden: 6, 5, 10, 9, 1, 13, 12, 2, 8, 3, 11, 7, 4.
 * 
 * 
 * */


void menu() {
    cout<<"Precione la opcion deseada:" <<endl<<endl;
    cout<<"1-Insertar enteros en el Arbol." <<endl;
    cout<<"2-Eliminar datos del Arbol." <<endl;
    cout<<"3-Cantidad de nodos." <<endl;
    cout<<"4-Imprimir inOrden." <<endl;
    cout<<"5-Imprimir preOrden." <<endl;
    cout<<"6-Imprimir posOrden." <<endl;
    cout<<"0-Salir." <<endl;
}



int main() {
	
	arbol *oArbol = new arbol();
    int datoArbol, retorno, nuevoDato;
    int array[30];   
    
    srand (time(NULL));
    
    for (int i=0;i<30;i++){
		array[i] = rand() % 400 + 100;
		cout << "-"<< array[i] << "-";
	}
	cout << endl;
	
    for (int i=0;i<30;i++){
            oArbol->hacerArbol(oArbol->getRaiz(), array[i]);
		
	}
		
	oArbol->imprimirInOrden(oArbol->getRaiz());
cout << endl;

    return 0;

}
