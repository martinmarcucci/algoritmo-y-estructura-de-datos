#include <iostream>
#include <cstdlib>
#include "classCola.hpp"

using namespace std;

Cola::Cola() {
    nCabeza = NULL;
    nCola = NULL;
}
Cola::Cola(int d) {
    nCabeza = new nodo(d);
    nCola = nCabeza;
}
void Cola::encolar(int d) {
    nodo *nuevo = new nodo(d);
    if (esVacia())
        nCabeza = nuevo;
    else
        nCola->insertarNodo(nuevo);
    nCola = nuevo;
}
int Cola::desencolar() {
    nodo *nuevo;
    int valor;

    nuevo = nCabeza;
    if (esVacia())
        return 0;
    nCabeza = nCabeza->getProximo();
    valor = nuevo->getDato();
    delete (nuevo);

    if (esVacia())
        nCola = NULL;

    return valor;
}

void Cola::imprimir() {
    nodo *cabeza = nCabeza;
    if (esVacia())
        cout << "Cola vacia" << endl;
    else {
        while (cabeza != NULL) {
            cout << "->" << cabeza->getDato();
            cabeza = cabeza->getProximo();
        }
    }
}

