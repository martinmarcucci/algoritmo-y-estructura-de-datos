#include <iostream>
#include <cstdlib>
#include "classNodo.hpp"

#ifndef CLASSPILA_H
#define CLASSPILA_H

class Cola {
private:
    nodo *nCabeza;
    nodo *nCola;
public:
    Cola();
    Cola(int d);
    void encolar(int d);
    int desencolar();
    int esVacia() {
        return nCabeza == NULL;
    }
    ;
    void imprimir();
    ~Cola() {
    }
    ;
};

#endif
