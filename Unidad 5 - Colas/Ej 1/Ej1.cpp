/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 5.1
 *
 * Enunciado:
 *
 *       Escribir un programa en C++, que lea cierta cantidad de enteros y luego determine: Cuál es
 *       el valor mayor, el valor menor y el promedio de todos los datos.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */


#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "classCola.hpp"

using namespace std;
int pedir_dato();
void calcular(Cola *co);

int main(void) {
    int tmp;
    Cola *col = new Cola();

    cout << "Llenar datos enteros, ingrese -1 para salir" << endl;

    cin >> tmp;
    while (tmp > 0) {
        col->encolar(tmp);
        cin >> tmp;
    }

    calcular(col);
    system("pause");
}


int pedir_dato() {
    int tmp;
    cout << "Ingrese un dato a insertar" << endl;
    cin >> tmp;
    return tmp;
}

void calcular(Cola *co) {
    int tmp, max, min, acu,cont=0;

    if (co->esVacia()) {
        cout << "esta vacia" << endl;
    } else {
        max = co->desencolar();
        min = max;
        acu = max;
        cont++;
        while (!co->esVacia()) {
            tmp = co->desencolar();
            if (tmp > max)
                max = tmp;
            if (tmp < min)
                min = tmp;

            acu += tmp;
            cont++;
        }
        cout << "Max: "<< max<<endl;
        cout << "Min: "<< min<<endl;
        cout << "Promedio: "<< ((float)acu/(float)cont)<<endl;

    }
}
