#include <iostream>
#include <cstdlib>
#include "classCola.hpp"

using namespace std;

Cola::Cola() {
    nCabeza = NULL;
    nCola = NULL;
    tamanio = 0;
}
Cola::Cola(int d) {
    nCabeza = new nodo(d);
    nCola = nCabeza;
    tamanio = 1;
}
void Cola::encolar(int d) {
    tamanio++;
    nodo *nuevo = new nodo(d);
    if (esVacia())
        nCabeza = nuevo;
    else
        nCola->insertarNodo(nuevo);
    nCola = nuevo;
}
int Cola::desencolar() {
    nodo *nuevo;
    int valor;

    nuevo = nCabeza;
    if (esVacia())
        return 0;
    nCabeza = nCabeza->getProximo();
    valor = nuevo->getDato();
    delete (nuevo);

    if (esVacia())
        nCola = NULL;

    tamanio--;
    return valor;
}

void Cola::imprimir() {
    nodo *cabeza = nCabeza;
    if (esVacia())
        cout << "Cola vacia" << endl;
    else {
        while (cabeza != NULL) {
            cout << "->" << cabeza->getDato();
            cabeza = cabeza->getProximo();
        }
    }
}

