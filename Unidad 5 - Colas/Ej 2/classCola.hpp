#include <iostream>
#include <cstdlib>
#include "classNodo.hpp"

#ifndef CLASSPILA_H
#define CLASSPILA_H

class Cola {
private:
    nodo *nCabeza;
    nodo *nCola;
    int tamanio;
public:
    Cola();
    Cola(int d);
    void encolar(int d);
    int desencolar();
    int esVacia() {
        return nCabeza == NULL;
    }
    ;
    int tam() {
        return tamanio;
    };
    void imprimir();
    ~Cola() {
    }
    ;
};

#endif
