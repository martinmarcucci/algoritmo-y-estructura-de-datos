/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 5.2
 *
 * Enunciado:
 *
 *       Dise�e un programa que sea capaz de leer dos colas y mediante un mensaje indicar si son
 *       iguales. Nota: los elementos constitutivos de las colas son caracteres.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <iostream>
#include "classCola.hpp"

using namespace std;

void leerCola(Cola *cola);
void verifica(Cola *cola1, Cola *cola2);
int main() {

    Cola *cola1 = new Cola();
    Cola *cola2 = new Cola();
    cout<<"Leyendo cola 1..."<<endl;
    leerCola(cola1);
    cout<<"Leyendo cola 2..."<<endl;
    leerCola(cola2);
    verifica(cola1,cola2);
    cout<<endl;
    system("pause");

}


void leerCola(Cola *cola) {
    char aux;
    bool control=true;
    int op;
    do {
        cout<<endl;
        if(cola->esVacia()) {
            cout<<"1)ENCOLAR"<<endl;
        } else {
            cout<<"1)ENCOLAR"<<endl;
            cout<<"2)DESENCOLAR"<<endl;
            cout<<"3)FINALIZAR"<<endl;
        }
        cin>>op;
        cout<<endl;

        switch(op) {
        case 1:
            cout<<"Ingrese el caracter a encolar"<<endl;
            cin>>aux;
            cola->encolar(aux);
            break;
        case 2:
            aux= cola->desencolar();
            cout<<"Elemento desencolado: "<<aux;
            break;
        case 3:
            cout<<"Lectura Finalizada"<<endl;
            control=false;
            break;
        }
    } while(control);
}

void verifica(Cola *cola1, Cola *cola2) {

    bool ver=true;

    if(cola1->tam()==cola2->tam()) {
        while(!cola1->esVacia())
            if(cola1->desencolar()!=cola2->desencolar())
                ver=false;
        if(ver)
            cout<<"LAS COLAS SON IGUALES"<<endl;
        else
            cout<<"LAS COLAS SON DIFERENTES"<<endl;
    } else
        cout<<"LAS COLAS SON DIFERENTES"<<endl;
}

