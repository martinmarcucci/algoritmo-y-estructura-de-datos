/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 4.3
 *
 * Enunciado:
 *
 *     Escribir una función Reemplazar que tenga como argumentos una pila con tipo de elemento
 *     int y dos valores int: nuevo y viejo de forma que si el segundo valor aparece en algún lugar
 *     de la pila, sea reemplazado por el segundo.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "classPila.hpp"

using namespace std;
void Buscar(Pila *miPila, int b);

int main(void) {
    Pila *miPila= new Pila();
    miPila->push(3);
    miPila->push(4);
    miPila->push(5);
    miPila->push(3);
    miPila->push(7);
    miPila->push(8);

    int b;

    cout<<"Ingrese el valor que desea buscar: ";
    cin>>b;
    Buscar (miPila, b);



    //system("pause");
}



void Buscar(Pila *miPila, int b) {
    Pila *miPila2= new Pila();
    int f=0;
    while (!miPila->esVacia()) {
        miPila2->push(miPila->Pop());
        if(b==miPila2->Pop()) {
            f++;
        }
    }

    cout<<"El valor se enuentra "<< f <<" veces en la pila"<<endl;
    if(f==0) cout<<endl<<"EL VALOR NO SE ENCUENTRA EN LA PILA"<<endl;
}


