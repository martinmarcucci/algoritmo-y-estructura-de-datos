#include <iostream>
#include <cstdlib>
#include "classPila.hpp"

bool Pila::esVacia() {
    return (nPila == NULL) ? true : false;
}


Pila::Pila() {
    nPila = NULL;
    size = 0;
}

Pila::Pila(int d) {
    nPila = new nodo(d);
    size = 1;
}

void Pila::push(int d) {
    nodo *nNodo = new nodo(d);
    nNodo->insertarNodo(nPila);
    size++;
    nPila = nNodo;
}

void Pila::push(nodo *topPila, int d) {
    nodo *nNodo = new nodo(d);
    nNodo->insertarNodo(topPila);
    size++;
    topPila = nNodo;
}

int Pila::Pop() {
    nodo *nodoPila;
    int v;
    nodoPila = nPila;
    if (esVacia())
        return 0;
    nPila = nPila->getProximo();
    v = nodoPila->getDato();
    delete (nodoPila);
    size--;
    return v;
}

