/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 4.2
 *
 * Enunciado:
 *
 *     Escriba un programa que introduzca una palabra y utilice una pila para imprimir la misma
 *        palabra invertida.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "classPila.hpp"

using namespace std;

int menu();
int pedir_dato() ;
int comparar_pilas(Pila *pla, Pila *plb) ;


int main(void) {
    int a;
    Pila *pilaA = new Pila();
    Pila *pilaB = new Pila();

    a = menu();
    while (a > 0 && a < 5) {
        switch (a) {
        case 1:
            pilaA->push(pedir_dato());
            break;
        case 2:
            pilaB->push(pedir_dato());
            break;
            break;
        case 3:
            pilaA->Pop();
            break;
        case 4:
            pilaB->Pop();
            break;
        }

        cout << endl<< endl<< endl << "Iguales: " << comparar_pilas(pilaA, pilaB) << endl;
        a = menu();
    }

    system("pause");
}

int menu() {
    int dato;
    cout << "  Ingrese la opcion" << endl;
    cout << "  ======= == ======" << endl << endl;

    cout << "1) Agregar a Pila   A" << endl;
    cout << "2) Agregar a Pila   B" << endl;
    cout << "3) Quitar de Pila  A" << endl;
    cout << "4) Quitar de Pila  B" << endl;
    cout << endl;
    cout << "Otro valor salir" << endl;

    cin >> dato;

    if (dato < 1 || dato > 4)
        dato = -1;
    return dato;
}

int pedir_dato() {
    int tmp;
    cout << "Ingrese un dato a insertar" << endl;
    cin >> tmp;
    return tmp;
}

int comparar_pilas(Pila *pla, Pila *plb) {
    int iguales = 1;
    Pila *A = new Pila();
    Pila *B = new Pila();

    int auxA, auxB;

    if (pla->getTamano() == plb->getTamano()) {
        while (!pla->esVacia() && iguales == 1) {
            auxA = pla->Pop();
            auxB = plb->Pop();
            if (auxA != auxB)
                iguales = 0;
            A->push(auxA);
            B->push(auxB);
        }

        while (!A->esVacia() ) {
            pla->push(A->Pop());
            plb->push(B->Pop());
        }
    } else
        iguales = 0;
    return iguales;
}
