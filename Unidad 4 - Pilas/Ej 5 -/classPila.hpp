#include <iostream>
#include <cstdlib>
#include "classNodo.hpp"


#ifndef CLASSPILA_H
#define CLASSPILA_H

class Pila {
private:
    nodo *nPila;
    int size;
public:
    Pila();
    Pila(int d);
    void push(int);
    void push(nodo*, int);
    int Pop();
    int tam();
    bool esVacia() ;
//            void buscar(int);
//            void reemplazar(int,int)
//            void imprimePila();
    int getTamano() {
        return size;
    }
    ;
    nodo *getNodo() {
        return nPila;
    }
    ;
    ~Pila() {
    }
    ;
};

#endif
