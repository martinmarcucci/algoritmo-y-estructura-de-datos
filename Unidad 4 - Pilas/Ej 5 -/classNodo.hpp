#ifndef CLASSNODO_H
#define CLASSNODO_H

class nodo {
    int dato;
    nodo *proximo;

public:
    nodo() {
        dato = 0;
        proximo = NULL;
    }
    ;
    nodo(int d);
    int getDato() {
        return dato;
    }
    ;
    nodo *getProximo() {
        return proximo;
    }
    ;
    void detDato(int d);
    void insertarNodo(nodo *nuevoNodo);
};

#endif
