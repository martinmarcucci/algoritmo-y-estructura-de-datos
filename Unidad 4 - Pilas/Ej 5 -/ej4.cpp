/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 4.4
 *
 * Enunciado:
 *
 *     Escriba una función que indique si dos pilas son iguales. Se entiende que
 *  dos pilas son iguales cuando sus elementos son idénticos y aparecen en el mismo orden.
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */

#include <stdio.h>
#include <iostream>
#include <stdlib.h>

#include "classPila.hpp"

using namespace std;
void verifipila(Pila *pi1, Pila *pi2) ;

int main(void) {
    Pila *miPila1= new Pila();
    miPila1->push(3);
    miPila1->push(4);
    miPila1->push(5);
    miPila1->push(3);
    miPila1->push(7);
    miPila1->push(5);

    Pila *miPila2= new Pila();
    miPila2->push(3);
    miPila2->push(4);
    miPila2->push(5);
    miPila2->push(3);
    miPila2->push(7);
    miPila2->push(8);

    verifipila (miPila1, miPila2);

    //system("pause");
}


void verifipila(Pila *pi1, Pila *pi2) {

    bool iguales=true;

    if(pi1->tam()==pi2->tam()) {
        while(!pi1->esVacia())
            if(pi1->Pop()!=pi2->Pop())
                iguales=false;
    } else
        iguales=false;

    if(iguales)
        cout<<"LAS PILAS SON IGUALES"<<endl;
    else
        cout<<"LAS PILAS SON DIFERENTES"<<endl;
}
