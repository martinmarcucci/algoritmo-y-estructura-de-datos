/*
 *             ALGORITMOS Y ESTRUCTURA DE DATOS
 * Ejercicio: 4.1
 *
 * Enunciado:
 *
 *       Realice un programa en C++ que llene una pila con las letras del alfabeto y las vaya
 *       sacando.
 *           Inicial: F->E->D->C->B->A
 *           Final: E->D->C->B->A
 *
 * Alumno: Ricardo Martin Marcucci
 *
 * */


#include <string.h>
#include <iostream>

#include "classPila.hpp"

using namespace std;

int main(void) {

    Pila *nvaPila= new Pila();
    char a[255];

    cout << "\nIngrese una palabra"<<endl;
    cin>>a;

    for(unsigned int i=0; i<strlen(a); i++)
        nvaPila->push(a[i]);

    while(!nvaPila->esVacia())
        cout<<nvaPila->Pop();
    // nvaPila->imprimePila();
    cout<<endl;


    system("pause");
}


